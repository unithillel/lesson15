<?php
abstract class CRMEntity{
	public static $propertie = 1;
	const NOT_ACTIVE = 0;
	const ACTIVE = 1;
	const FINISHED = 2;

	protected $title;
	protected $type;

	public static function getType(){
		return 'default';
	}

	public function __construct($title){
		if(strlen($title) < 1){
			throw new Exception("Title should be longer than 1 symbol");	
		}

		$this->title = $title;
		//self == Ссылка на класс в котором определен текущий метод
		//static == Ссылка на вызывающий метод класс
		$this->type = static::getType();
	}

}